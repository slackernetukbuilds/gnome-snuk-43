#!/bin/sh

# Slackware build script for gnome-shell-extension-arc-menu 

# Copyright 2022 Frank Honolka <slackernetuk@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=gnome-shell-extension-arc-menu
SRCNAM=ArcMenu
VERSION=${VERSION:-38}
BUILD=${BUILD:-1}
TAG=${TAG:-_snuk}
_uuid=arcmenu@arcmenu.com
# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/snuk}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $SRCNAM-v$VERSION
tar xvf $CWD/$SRCNAM-v$VERSION.tar.gz
cd $SRCNAM-v$VERSION
sed -i 's/42/43/g' metadata.json || exit 1
make VERSION="$VERSION" _build
cd _build || exit 1
_destdir="${PKG}/usr/share/gnome-shell/extensions/${_uuid}"
install -Dm644 metadata.json *.js *.css -t ${_destdir}/ || exit 1
cp -r --no-preserve=ownership,mode media "${_destdir}" || exit 1
cp -r --no-preserve=ownership,mode menulayouts "${_destdir}" || exit 1
cp -r --no-preserve=ownership,mode settings "${_destdir}" || exit 1
cp -r --no-preserve=ownership,mode searchProviders "${_destdir}" || exit 1
install -Dm644 schemas/org.gnome.shell.extensions.arcmenu.gschema.xml -t ${PKG}/usr/share/glib-2.0/schemas/ || exit 1 

cd locale
for locale in */
do
 install -Dm644 -t "${PKG}/usr/share/locale/${locale}/LC_MESSAGES" "${locale}/LC_MESSAGES"/*.mo || exit 1 
done
cd ../..

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
DOCS="COPYING README.md"
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION || exit 1
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
