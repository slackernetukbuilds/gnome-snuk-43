#!/bin/sh

# Slackware build script for epiphany

# Copyright 2022 Frank Honolka (slackernetuk@gmail.com)
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
PRGNAM=gnome-software
VERSION=${VERSION:-43.2}
BUILD=${BUILD:-1}
TAG=${TAG:-_snuk}
PLUG_version=19
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

TMP=${TMP:-/tmp/snuk}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}
CWD=$(pwd)
JOBS=${JOBS:-" -j$(expr $(getconf _NPROCESSORS_ONLN) \* 4 ) "}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$PRGNAM-$VERSION.tar.?z
cd $PRGNAM-$VERSION
zcat $CWD/0001-crash-with-broken-theme.patch.gz | patch -p1 || exit
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
meson build \
 --prefix=/usr \
 --libdir=/usr/lib${LIBDIRSUFFIX} \
 --sysconfdir=/etc \
 --localstatedir=/var \
 --mandir=/usr/man \
 --buildtype=release \
 -Dpackagekit=false \
 -Dexternal_appstream=false \
 -Dsoup2=true \
 -Dflatpak=true \
 -Dsnap=false \
 -Dfwupd=false \
 -Drpm_ostree=false \
 -Dwebapps=true \
 -Dman=true \
 -Dhardcoded_foss_webapps=false \
 -Dhardcoded_proprietary_webapps=false \
 -Dtests=false 
meson compile -C build 
meson install -C build --destdir $PKG 

rm $PKG/usr/lib$LIBDIRSUFFIX/gnome-software/plugins-$PLUG_version/libgs_plugin_dpkg.so || exit 1
rm $PKG/usr/lib$LIBDIRSUFFIX/gnome-software/plugins-$PLUG_version/libgs_plugin_fedora-langpacks.so || exit 1
rm $PKG/usr/lib$LIBDIRSUFFIX/gnome-software/plugins-$PLUG_version/libgs_plugin_fedora-pkgdb-collections.so || exit 1

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

if [ -d $PKG/usr/man ]; then
find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
fi

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
DOCS="AUTHORS COMMITMENT COPYING NEWS README.md RELEASE.md"
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION

cat $CWD/$(basename $0) > $PKG/usr/doc/$PRGNAM-$VERSION/$(basename $0)

mkdir -p $PKG/install
cp $CWD/slack-desc $PKG/install
cp $CWD/doinst.sh $PKG/install

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
