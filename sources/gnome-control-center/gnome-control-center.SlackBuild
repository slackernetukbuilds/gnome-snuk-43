#!/bin/sh

# Slackware build script for gnome-control-center

# Copyright 2016, 2021 Rafael Tavares (mdrafaeltavares@gmail.com) Bahia, Brazil.
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=gnome-control-center
VERSION=${VERSION:-43.2}
BUILD=${BUILD:-1}
TAG=${TAG:-_snuk}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/snuk}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}
JOBS=${JOBS:-" -j$(expr $(getconf _NPROCESSORS_ONLN) \* 2 ) "}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xfv $CWD/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION
#zcat $CWD/distro-logo.patch.gz | patch -p1 || exit
#
# Bug references for both the following two patches:
# https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2025
# https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2028
# https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/5767
# https://bugzilla.redhat.com/show_bug.cgi?id=2118152
# Fix blank-screen and power button behavior settings not applying
# https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1439 
#zcat $CWD/1439.patch.gz | patch -p1 || exit
# Fix primary monitor selection not working
# https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1440
#zcat $CWD/1440-rediffed.patch | patch -p1 || exit
chown -R root:root .

# libgnome-volume-control subproject can be builded with shared library and alsa.
#sed -i "s#'static=true',#'static=false','pkglibdir=/usr/lib${LIBDIRSUFFIX}',#" meson.build || exit 1
#sed -i 's/alsa=false/alsa=true/' meson.build || exit 1

mkdir -p build
cd build

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
meson .. \
 --prefix=/usr \
 --buildtype=release \
 --libdir=lib${LIBDIRSUFFIX} \
 --libexecdir=/usr/libexec \
 --sysconfdir=/etc \
 --infodir=/usr/info \
 --mandir=/usr/man \
 --localstatedir=/var \
 -Ddistributor_logo=/usr/share/pixmaps/slackware_logo_med.png \
 -Ddark_mode_distributor_logo=/usr/share/pixmaps/slackware_whitelogo_med.png \
 -Dmalcontent=true \
 -Ddefault_library=shared \
 -Ddocumentation=true

# -Dcheese=true \
# -Dibus=true \
# -Dprivileged_group=wheel \
# -Dsnap=false \
# -Dtests=false \
# -Dtracing=false \
# -Dwayland=true \
# -Dprofile=default \
# -Dmalcontent=true

ninja $JOBS || ninja
DESTDIR=$PKG ninja install

install -Dm 644 $CWD/slackware_logo_med.png -t $PKG/usr/share/pixmaps/
install -Dm 644 $CWD/slackware_whitelogo_med.png -t $PKG/usr/share/pixmaps/

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

cd ..
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a COPYING NEWS README.md \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$(basename $0) > $PKG/usr/doc/$PRGNAM-$VERSION/$(basename $0)

mkdir -p $PKG/install
cp $CWD/slack-desc $PKG/install

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
